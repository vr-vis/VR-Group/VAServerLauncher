# VAServerLauncher

This holds prebuild VAServers for Windows which can be launched from Unreal using a python script. Additionally, this allows to specify all rendering settings for VA within the Unreal project, while costumizing the hardware and reproduction settings on the target machine running the VAServer.

## Requiremirents
- Python version 3.X
- An Unreal project using the [Unreal VA plugin](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/unreal-va-plugin)

## Quick guide for local usage
*VAServer and Unreal project are running on same computer*

VAServerLauncher:
- Checkout this repository to your computer, use the default folder name `VAServerLauncher`

Unreal project:
- Your project (e.g. this [demo-project](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/demos/unreal-va-demo)) should be placed next to the `VAServerLauncher` folder
- Make sure the `Use VAServerLauncher` option of your `VAReceiverActor` is enabled
- Run the project: The VAServerLauncher should be automatically started launching a VAServer instance. Unreal connets to this instance and an audio stream should be audible (default output should be headphones on channels 1 & 2).

## Quick guide for remote usage
*VAServer and Unreal project on different computers*

Computer running VAServer:
- Checkout this repository
- Manually start the VAServerLauncher by running ``./LaunchScripts/VirtualAcousticsStarterServer.py`` via python

Computer running Unreal:
- Make sure the `Use VAServerLauncher` option of your `VAReceiverActor` is enabled
- Run the project: The VAServerLauncher should start a VAServer instance. Unreal connets to this instance and an audio stream should be audible (default output should be headphones on channels 1 & 2).


## Launcher configuration

The VAServerLauncher can be configured by adjusting the settings in `./LaunchScripts/VirtualAcousticStarterConfig.json`.

| Setting | Description | Default |
|---------|-------------|---------|
| sLocalIP | IP of this computer. Specify for remote usage | "localhost"
| nLauncherPort | Port used for communication with launcher | 41578 |
| nVAServerPort | Port used for communication with VAServer | 12340 |
| nWaitForVAServerStart | Time to wait for VAServer to start [seconds] | 3 |
| sVASetupIni | Name of VA ini-file with [audio hardware](#audio-hardware) settings located in `conf` folder | "VASetup.Launcher.ini" |
| lsBinauralReproductionModules | [Reproduction modules](#audio-reproduction) used for [binaural](#unreal-project-configuration) input signals | ["TalkthroughHP"] |
| lsAmbisonicsReproductionModules | [Reproduction modules](#audio-reproduction) used for [ambisonics](#unreal-project-configuration) input signals | ["AmbisonicsBinauralMixdown"] |
| lsCustomReproductionModules | [Reproduction modules](#audio-reproduction) used for [custom](#unreal-project-configuration) purposes (e.g. multiple renderers with different signal types) | e.g. ["TalkthroughHP", "AmbisonicsBinauralMixdown"] |
| dVAServerDirectories | Dictionary connecting a `VAServer Version ID` with the respective directory | e.g. `"2022.a": "../v2022.a"`

If you want to use other VAServer versions than provided, you can extend the `dVAServerDirectories` dictionary.

## VAServer configuration

A set of .ini files is provided in the `./conf` directory. The VAServerLauncher will automatically combine them before starting the VAServer. For this to work, you should **not** change those files with the following exceptions.

### Audio hardware
The audio hardware settings are specific to each computer and typically have to be modified in the VASetup ini file (see `sVASetupIni` in [Launcher config](#launcher-configuration), default `VASetup.Launcher.ini`). Those include [audio driver](https://www.virtualacoustics.org/VA/documentation/configuration/#audio-driver-configuration) and [output device](https://www.virtualacoustics.org/VA/documentation/configuration/#audio-hardware-configuration) settings including the respective channels (e.g. headphones on channels 1 & 2).

### Audio reproduction
The audio [reproduction settings](https://www.virtualacoustics.org/VA/documentation/reproduction/) can to be modified in the `VAReproduction.Prototype.ini`. VA uses reproduction modules to specify how the audio stream is processed before being sent to the output devices. For examples binaural signals can be reproduced using a direct routing to headphones or using cross-talk cancellation (CTC) and loudspeakers.

Per default the following reproduction modules are defined
- TalkthroughHP
- Headphones
- CTC
- TalkthroughLS
- AmbisonicsLS
- AmbisonicsBinauralMixdown

You can add additional modules, if you want. However, you should **not** rename the existing ones. Furthermore, all modules must be disbaled [Enabled = false]. The VAServerLauncher will automatically enable the reproduction modules specified in the `VirtualAcousticStarterConfig.json` (see [Launcher configuration](#launcher-configuration)) when starting the VAServer.

In any case, especially for the loudspeaker-based modules, you will have to update the `Outputs` setting according to what you defined in the `VASetup.Launcher.ini`.

### Audio rendering
The [rendering settings](https://www.virtualacoustics.org/VA/documentation/rendering/) specify, how audio is rendered based on the virtual sources and the receiver in your scene. Typically, this should live in your Unreal project (see [Unreal project configuration](#unreal-project-configuration)). However, the file `VARenderer.Default.ini` is provided in case the Unreal project does not provide such a file.


## Unreal project configuration
When working with the [Unreal VA plugin](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/unreal-va-plugin), all settings regarding the VAServerLauncher can be found in the `VAReceiverActor` under the `VAServer Launcher` category.

| Setting | Description | Default |
|---------|-------------|---------|
| Use VAServerLauncher | Switch to de-/activate usage | True
| VAServerLauncher Port | Port used for communication with launcher | 41578 |
| VAServer Version ID | ID used to specify which VAServer version should be started. This must match a value in the dictionary `dVAServerDirectories` in the [Launcher config](#launcher-configuration). | "2022.a" |
| VARenderer.ini file | Filename of the ini file including the audio rendering settings for this project. If specified, the file will be sent to the VAServerLauncher on startup. | "" |
| Reproduction input signal type | Depending on this setting a different set of reproduction modules (see [Launcher config](#launcher-configuration)) is used by the VAServerLauncher, the group [Binaural / Ambisonics / Custom] | Binaural |