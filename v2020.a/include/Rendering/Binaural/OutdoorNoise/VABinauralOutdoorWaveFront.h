/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_BINAURAL_OUTDOOR_WAVE_FRONT
#define IW_VACORE_BINAURAL_OUTDOOR_WAVE_FRONT

// VA includes
#include <VA.h>
#include <VAPoolObject.h>
#include "../Clustering/VABinauralWaveFront.h"
#include "../Clustering/Receiver/VABinauralClusteringReceiver.h"

// ITA includes
#include <ITAIIRCoefficients.h>
#include <ITAIIRUtils.h>
#include <ITAInterpolation.h>
#include <ITASIMOVariableDelayLineBase.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <ITAIIRFilterEngine.h>

// Forwards
class CVASoundSourceState;
class CVASoundSourceDesc;
class CVASharedMotionModel;
class CITAVariableDelayLine;
class ITASampleBuffer;
class CVABinauralOutdoorSource;

//! Represents a wave front emitted from a sound source for an outdoor scenario
/**
  * @henry: The implementation of this class is not nice (yet), but focus on the DSP output and leave it as is.
  */
class CVABinauralOutdoorWaveFront : public IVABinauralWaveFront, public CVAPoolObject
{
public:

	struct Config
	{
		bool bMotionModelLogInput;
		bool bMotionModelLogEstimated;

		double dMotionModelWindowSize;
		double dMotionModelWindowDelay;

		int iMotionModelNumHistoryKeys;

		double dSampleRate;
		int iBlockLength;

		int iIIRFilterOrder;
		int iFIRFilterLength;
		int iFilterDesignAlgorithm;
		int iBufferSize;

		int iSwitchingAlgorithm;
	};

	const CVABinauralOutdoorWaveFront::Config oConf;	

	CVABinauralOutdoorWaveFront( const CVABinauralOutdoorWaveFront::Config& conf );
	~CVABinauralOutdoorWaveFront();

	void PreRequest();
	void PreRelease();

	void GetOutput( ITASampleBuffer* pfLeftChannel, ITASampleBuffer* pfRightChannel );

	void SetParameters( const CVAStruct &oInArgs );

	void SetFilterCoefficients(const ITABase::CThirdOctaveGainMagnitudeSpectrum &oMags );

	void SetDelay( const int delay );
	void SetGain( float fGain );
	void SetMotion();

	void SetSource(CVABinauralOutdoorSource* source);

	void SetReceiver(CVABinauralClusteringReceiver* sound_receiver);

	void setITDDifference(const float itdDiff);
		
	bool GetValidWaveFrontOrigin() const;
	bool GetValidClusteringPose() const;
	void SetClusteringMetrics( const VAVec3& v3Pos, const VAVec3& v3View, const VAVec3& v3Up, const VAVec3& v3PrincipleDirectionOrigin );
	void SetWaveFrontOrigin( const VAVec3& v3Origin );
	VAVec3 GetWaveFrontOrigin() const;
	
	int AddReference();
	int RemoveReference();

	void SetAudible( bool bAudible );

private:

	CVABinauralOutdoorSource* m_pSource;
	CVABinauralClusteringReceiver* m_pReceiver;

	double m_dCreationTimeStamp;

	ITABase::CThirdOctaveGainMagnitudeSpectrum m_oMags;
	ITADSP::CFilterCoefficients m_iIIRFilterCoeffs;
	CITAIIRFilterEngine m_oIIRFilterEngine;

	CITASIMOVariableDelayLineBase* m_pSIMOVDL;
	IITASampleInterpolationRoutine* m_pInterpolationRoutine;

	int m_iCursorID;

	ITASampleBuffer m_sbTemp;
	ITASampleBuffer m_sbInterpolated;

	int m_iITDDifference;
	bool m_bAudible; //!< Indicates a valid but inaudible path
	float m_fGainCur;
	float m_fGainNew;

	bool m_bWaveFrontOriginSet;
	bool m_bClusteringPoseSet;

	VAVec3 m_v3ClusteringPos;
	VAVec3 m_v3ClusteringView;
	VAVec3 m_v3ClusteringUp;
	VAVec3 m_v3ClusteringPrincipleDirectionOrigin;

	VAVec3 m_v3WaveFrontOrigin;

#ifdef BINAURAL_OUTDOOR_NOISE_INSOURCE_BENCHMARKS
	ITAStopWatch m_swGetOutput;
	ITAStopWatch m_swIIRFilter;
	ITAStopWatch m_swVDL;
	ITAStopWatch m_swInterpolation;
#endif

};

#endif // IW_VACORE_BINAURAL_OUTDOOR_WAVE_FRONT
