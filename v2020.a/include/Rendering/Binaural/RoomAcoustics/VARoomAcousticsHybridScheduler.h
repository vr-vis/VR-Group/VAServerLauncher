/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_ROOMACOUSTICS_HYBRID_SCHEDULER
#define IW_VACORE_ROOMACOUSTICS_HYBRID_SCHEDULER

#if (VACORE_WITH_RENDERER_BINAURAL_ROOM_ACOUSTICS==1)

#include <ITASimulationScheduler/Interfaces.h>
#include <ITASimulationScheduler/LocalScheduler.h>

// ITA includes
#include <ITASampleFrame.h>

// Vista includes
#include <VistaInterProcComm/Concurrency/VistaThreadEvent.h>
#include <VistaInterProcComm/Concurrency/VistaThreadLoop.h>
#include <VistaInterProcComm/Concurrency/VistaPriority.h>

// 3rdParty includes
#include <tbb/concurrent_queue.h>

// STL includes
#include <atomic>
#include <cassert>
#include <list>

// VA includes
#include <VAObject.h>

// Forward declarations
class CVACoreImpl;
class CVARoomAcousticsSimulationTask;

namespace ITASimulationScheduler
{
	namespace RAVEN
	{
		class IRavenNetClient;
	}
}


class CVARavenHybridScheduler : public ITASimulationScheduler::ISimulationSchedulerInterface, public VistaThreadLoop, public CVAObject {
public:
	//! Konstruktor
	CVARavenHybridScheduler( CVACoreImpl*, const std::string& sRavenDataBasePath, const std::string& sServerIP );

	//! Destruktor
	virtual ~CVARavenHybridScheduler();

	CVAStruct CallObject( const CVAStruct& oArgs );
	CVAObjectInfo GetObjectInfo() const;

	void Reset();
	void LoadScene( const std::string& sFileName );
	void AddTask(ITASimulationScheduler::CSimulationTask* pTask );
	bool AttachSimulationResultHandler(ITASimulationScheduler::ISimulationSchedulerResultHandler* );
	bool DetachSimulationResultHandler(ITASimulationScheduler::ISimulationSchedulerResultHandler* );

	bool GetProfilerStatus( CProfiler& oStatus );

	bool LoopBody();

	//! Bestimmt die Tasks, die durch den lokalen Scheduler abgearbeitet werden sollen
	/**
	  * \param viTaskDuties Siehe CSimulationTask->Simulationstyp
	  */
	void SetLocalFieldOfDuties( const std::vector< int >& viTaskDuties );
	
	//! Bestimmt die Tasks, die durch den entfernten Scheduler abgearbeitet werden sollen
	/**
	  * \param viTaskDuties Siehe CSimulationTask->Simulationstyp
	  */
	void SetRemoteFieldOfDuties( const std::vector< int >& viTaskDuties );

	ITASimulationScheduler::ISimulationSchedulerInterface* GetLocalScheduler() const;
	ITASimulationScheduler::ISimulationSchedulerInterface* GetRemoteScheduler() const;
	
private:
	CVACoreImpl* m_pCore;

	ITASimulationScheduler::ISimulationSchedulerInterface* m_pLocalScheduler;
	ITASimulationScheduler::ISimulationSchedulerInterface* m_pRemoteScheduler;

	ITASimulationScheduler::RAVEN::IRavenNetClient* m_pRavenNetClient;

	std::vector< int > m_viLocalFieldOfDuties;
	std::vector< int > m_viRemoteFieldOfDuties;

	std::list< ITASimulationScheduler::ISimulationSchedulerResultHandler* > m_lpTaskHandler; //!< Liste der Task-Handler

	typedef std::list< CVARoomAcousticsSimulationTask* > TaskList;
	typedef TaskList::iterator TaskListIt;
	typedef TaskList::const_iterator TaskListCit;

	tbb::concurrent_queue< CVARoomAcousticsSimulationTask* > m_qpNewTasks; // �bergabestruktur (non-blocking)
	TaskList m_lDSTasks;	// Interne Aufgabenlisten Direktschall
	TaskList m_lISTasks;	// Interne Aufgabenlisten Image Sources
	TaskList m_lRTTasks;	//!< Internal list of tasks for diffuse decay using ray tracing
	TaskList m_lARTasks;	//!< Internal list of tasks for diffuse decay using artificial reverb

	class CRavenSchedulerProfile;
	CRavenSchedulerProfile* m_pProfile; // Zeiger auf Profiler des Schedulers

	VistaThreadEvent m_evTrigger;

	std::atomic< bool > m_bStop;
	std::atomic< bool > m_bIndicateReset;

	//! Filtert und weist Tasks ab, die gar nicht erst zu den Schedulern geleitet werden m�ssen
	void FilterAndReplaceTasks( TaskList& , CVARoomAcousticsSimulationTask* );

	//! Weist einen Task sofort ab, ohne die Scheduler zu informieren
	/*
	 * �ber diesen Task werden weder lokaler noch entfernter Scheduler informiert,
	 * z.B. wenn ein neuerer Task einen internen verdr�ngt, der noch gar nicht an
	 * die Scheduler geleitet wurden, oder wenn ein Task w�hrend des Reset hinzugef�gt
	 * wird, oder wenn das 'Field of Duty' ge�ndert wird, etc.
	 *
	 */
	void ImmediatelyDiscard( CVARoomAcousticsSimulationTask* );
};

#endif // (VACORE_WITH_RENDERER_BINAURAL_ROOM_ACOUSTICS==1)

#endif // IW_VACORE_ROOMACOUSTICS_HYBRID_SCHEDULER
