/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_SIMPLE_CONVOLUTION
#define INCLUDE_WATCHER_ITA_SIMPLE_CONVOLUTION

#include <ITABaseDefinitions.h>

//! Diskrete Faltung im Zeitbereich durchf�hren
/**
 * Diese Funktion f�hrt die diskrete Faltung zweier Signale A und B im
 * Zeitbereich durch. Diese Operation hat eine Laufzeitkomplexit�t von
 * O(M*N) und ist nur effizient f�r Signale geringer L�nge. (F�r l�ngere
 * Signale sollen Blockfaltungsverfahren benutzt werden).
 *
 * Semantik: C(n) = sum( A(k)B(n-k), k=-inf..+inf )
 *
 * \param A Eingangssignal 1
 * \param M Anzahl Felder im Eingangssignal 1
 * \param B Eingangssignal 2
 * \param N Anzahl Felder im Eingangssignal 2
 * \param C Ausgangssignal
 * \param K Anzahl Felder im Ausgangssignal
 *
 * Hinweis: Das vollst�ndige Ausgangssignal hat eine L�nge von M+N-1 Feldern.
 *          Trotzdem darf die Anzahl Felder im Ausgangssignal geringer sein.
 *          In diesem Falle wird das Faltungsergebnis gek�rzt.
 */
void ITA_BASE_API conv( const float *A, int M, const float *B, int N, float *C, int K );
void ITA_BASE_API conv( const double *A, int M, const double *B, int N, double *C, int K );

#endif // INCLUDE_WATCHER_ITA_SIMPLE_CONVOLUTION
