/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_SAMPLE_FRAME
#define INCLUDE_WATCHER_ITA_SAMPLE_FRAME

// ITABase
#include <ITABaseDefinitions.h>
#include <ITASampleBuffer.h>

// STL
#include <vector>

/**
 * Ein SampleFrame in eine Menge von SamplePuffern
 */

class ITA_BASE_API ITASampleFrame
{
public:
	//! Standardkonstruktor
	/**
	 * Dieser Konstruktor erzeugt einen leeren Frame, ohne Kan�le und der L�nge 0.
	 */
	ITASampleFrame( );

	//! Konstruktor
	/**
	 * Erzeugt einen Frame der gew�nschten Anzahl Kan�le und L�nge, ohne Abtastrate.
	 * Standardm��ig werden Samples mit Nullen initialisiert.
	 * F�r manche Anwendungen ist im Sinne maximaler Geschwindigkeit
	 * sinnvoll diese Initialisierung auszulassen. Die Samples des Frame
	 * enthalten dann allerdings unbestimmte (mehr oder weniger zuf�llige) Werte.
	 *
	 * \param iChannels	Anzahl Kan�le
	 * \param iLength	L�nge [Anzahl Samples]
	 * \param bZeroinit	Frame mit Nullen initialisieren?
	 */

	explicit ITASampleFrame( const int iChannels, const int iLength, const bool bZeroinit = 0 );

	//! Kopierkonstruktor (Zeiger)
	/**
	 * Erzeugt einen unabh�ngigen Frame als Kopie des gegebenen Frames.
	 * Der neue Frame hat die selbe Anzahl Kan�le und L�nge und enth�lt
	 * die gleichen Werte wie der Quellframe.
	 *
	 * \param pSource Zeiger auf den Quellframe
	 */
	ITASampleFrame( const ITASampleFrame* pSource );

	//! Kopierkonstruktor (Referenz)
	/**
	 * Erzeugt einen unabh�ngigen Frame als Kopie des gegebenen Frames.
	 * Der neue Frame hat die selbe Anzahl Kan�le und L�nge und enth�lt
	 * die gleichen Werte wie der Quellframe.
	 *
	 * \param bSource Referenz auf den Quellframe
	 */
	ITASampleFrame( const ITASampleFrame& fSource );

	//! Ladekonstruktor (Audiodatei)
	/**
	 * Dieser Konstruktor erzeugt einen Frame indem er ihn aus einer Audiodatei l�dt.
	 *
	 * \note Im Fehlerfall wird eine ITAException ausgel�st.
	 */
	ITASampleFrame( const std::string& sFilePath );

	//! Destruktor
	virtual ~ITASampleFrame( );

	//! Ist der Frame leer?
	/**
	 * Ein Frame ist leer wenn er die L�nge 0 und/oder keine Kan�le hat
	 */
	bool empty( ) const;

	//! Anzahl Kan�le zur�ckgeben
	inline int channels( ) const { return GetNumChannels( ); };

	int GetNumChannels( ) const;

	// Deprecated
	int length( ) const;

	//! L�nge [Anzahl Samples] zur�ckgeben
	int GetLength( ) const { return length( ); };

	//! Frame initialisieren
	/**
	 * Mit dieser Methode wird ein Frame initialisiert. Dies bedeutet das seine
	 * Anzahl Kan�le und L�nge festgelegt wird und der Speicher alloziert wird.
	 * Falls ein Frame bereits initialisiert war, gehen alle vorherigen Daten verloren.
	 * Die Methode erm�glich bequemes programmieren, da Frames zun�chst per
	 * Standard-Konstruktor erzeugt werden k�nnen und erst sp�ter initialisiert werden.
	 *
	 * \param dSamplerate  Abtastrate [Hz] (0 => unbestimmt)
	 * \param iChannels	Anzahl Kan�le
	 * \param iLength	L�nge [Anzahl Samples]
	 * \param bZeroinit	Samples mit Nullen initialisieren?
	 */
	void Init( int iChannels, int iLength, bool bZeroinit );

	//! Legacy Init with small caps style
	inline void init( int iChannels, int iLength, bool bZeroinit ) { return Init( iChannels, iLength, bZeroinit ); };

	void Load( const std::string& sFilePath );
	void Load( const std::string& sFilePath, double& dSampleRate );
	void Store( const std::string& sFilePath, double dSampleRate = 44100.0f ) const;

	//! Speicher freigeben
	/**
	 * Gibt den f�r den Frame allozierten Speicher frei und setzt seine
	 * Anzahl Kan�le und L�nge auf 0. Hierbei gehen nat�rlich alle Daten verloren.
	 */
	void free( );

	//! Setzt alle Samples alles Kan�le auf den gegebenen Wert
	void fill( float fValue );

	//! Setzt die Samples aller Kan�le in einem Bereich auf den angegebenen Wert
	/**
	 * \param iOffset Startindex
	 * \param iCount  Anzahl Samples
	 * \param fFloat  Wert
	 */
	void fill( int iOffset, int iCount, float fValue );

	//! Setzt alle Samples aller Kan�le zu Null
	void zero( );

	//! Setzt einen Bereich von Samples aller Kan�le zu Null
	void zero( int iOffset, int iCount );

	//! Setzt Einheitsimpulse in jedem Kanal
	void identity( );

	//! Ein-/Ausblenden
	/**
	 * Blendet die Samples im Bereich [iOffset, iOffset+iCount] mit der
	 * angegebenen Blendfunktion ein.
	 */
	void fade( int iOffset, int iCount, int iFadeDirection, int iFadeFunction );

	//! Kreuzblenden
	/**
	 * F�hrt eine Kreuzblende von angebenen Frame in diesen Frame durch.
	 *
	 * F�r den Modus CROSSFADE_FROM gilt:
	 *
	 * Zun�chst werden iOffset Samples vom angebenen Puffer kopiert.
	 * Danach werden iCount Samples zwischen den beiden Puffern kreuzgeblendet.
	 * Dahinter folgen nur noch Samples dieses Puffers.
	 */
	void crossfade( const ITASampleFrame* psfSrc, int iOffset, int iCount, int iFadeDirection, int iFadeFunction );
	void crossfade( const ITASampleFrame& sfSrc, int iOffset, int iCount, int iFadeDirection, int iFadeFunction );

	//! Einh�llende
	/**
	/* Wendet eine lineare Einh�llende (envelope) auf den Frame an.
	*
	* \param fGain0	Startwert [0..1]
	* \param fGain1	Endwert [0..1]
	*/
	void envelope( float fGain0, float fGain1 );

	//! Samples aus einem anderen Frame in den Frame kopieren
	/**
	 * Kopiert iCount Samples aus angegebenen Frame beginnend bei Leseposition iSrcOffset
	 * in disen Frame, dort beginnend ab Schreibposition iDestOffset.
	 *
	 * \param psfSrc		Quellframe
	 * \param iCount		Anzahl zu kopierender Samples
	 * \param iSrcOffset	Leseposition im Quellframe
	 * \param iDestOffset	Schreibposition in diesem Frame
	 *
	 * \note Kein Schreiben �ber das Frameende hinaus!
	 * \note Beide Frames m�ssen die gleiche Anzahl Kan�le haben
	 */
	void write( const ITASampleFrame* psfSrc, int iCount, int iSrcOffset = 0, int iDestOffset = 0 );
	void write( const ITASampleFrame& sfSrc, int iCount, int iSrcOffset = 0, int iDestOffset = 0 );

	//! Zyklisches Schreiben
	/**
	 * Diese Methode erlaubt das zylische Schreiben von Samples in den Frame.
	 * Wenn die Leseanforderung das Ende der Frames �berschreitet wird das
	 * Lesen einfach am Anfang des Frames fortgesetzt. N�tzlich ist diese
	 * Funktionalit�t vorallem f�r das Implementieren von Ringpuffern.
	 * Semantik ansonsten wie read().
	 *
	 * \param psfSrc		Quellframe
	 * \param iCount		Anzahl zu kopierender Samples
	 * \param iSrcOffset	Leseposition im Quellframe
	 * \param iDestOffset	Schreibposition in diesem Frame
	 *
	 * \note Beide Frames m�ssen die gleiche Anzahl Kan�le haben
	 *
	 * TODO: Memory alignment f�r SSE?
	 */
	void cyclic_write( const ITASampleFrame* psfSrc, int iCount, int iSrcOffset = 0, int iDestOffset = 0 );
	void cyclic_write( const ITASampleFrame& sfSrc, int iCount, int iSrcOffset = 0, int iDestOffset = 0 );

	//! Cyclic shifting of samples
	/**
	 * @param [in] iCount Shifts the samples in frames by given count
	 */
	void CyclicShift( int iCount );

	//! In-place Addition: Jedem Sample einen konstanten Wert addieren
	void add_scalar( float fValue );

	//! In-place Subtraktion: Jedem Sample einen konstanten Wert subtrahieren
	void sub_scalar( float fValue );

	//! In-place Multiplikation: Jedes Sample mit einem konstanten Wert multiplizieren
	void mul_scalar( float fValue );

	//! In-place Division: Jedes Sample durch einen konstanten Wert dividieren
	void div_scalar( float fValue );

	// TODO: Bereiche Addieren usw.

	// Operatoren: Alle Kan�le mit einem SampleBuffer
	void add_buf( const ITASampleBuffer* psbSource );
	void sub_buf( const ITASampleBuffer* psbSource );
	void mul_buf( const ITASampleBuffer* psbSource );
	void div_buf( const ITASampleBuffer* psbSource );

	// Varianten mit Referenzen
	void add_buf( const ITASampleBuffer& sbSource );
	void sub_buf( const ITASampleBuffer& sbSource );
	void mul_buf( const ITASampleBuffer& sbSource );
	void div_buf( const ITASampleBuffer& sbSource );

	//! Paarweise alle Samples des gegebenen Blockes zu den Samples diesem addieren
	/*
	 * - M�ssen gleiche Anzahl Kan�le und L�ngen haben!
	 */
	void add_frame( const ITASampleFrame* psfSource );
	void add_frame( const ITASampleFrame* psfSource, int iNumSamples );
	void sub_frame( const ITASampleFrame* psfSource );
	void mul_frame( const ITASampleFrame* psfSource );
	void div_frame( const ITASampleFrame* psfSource );

	// Varianten mit Referenzen
	void add_frame( const ITASampleFrame& sfSource );
	void sub_frame( const ITASampleFrame& sfSource );
	void mul_frame( const ITASampleFrame& sfSource );
	void div_frame( const ITASampleFrame& sfSource );

	// mit �bergabe der Position - keine identische L�nge ben�tigt
	void add_frame_pos( const ITASampleFrame* psfSource, int iPos );

	// Kombinierte Operatoren

	// Werte eines anderen Frames mit einer Konstante multiplizieren und dann hierauf addieren
	// Semantik:
	// for i from 0 to iCount-1 do
	//   this[dest_offset + i] += (scalar * source[src_offset + i])
	//
	// iSrcOffset = Leseposition im Quellpuffer
	// iDestOffset = Schreibposition in diesem Puffer
	// iCount = Anzahl Samples
	void muladd_frame( const ITASampleFrame* psfSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount );
	void muladd_frame( const ITASampleFrame& sfSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount );


	//! Spitzenwert suchen
	/**
	 * Sucht den betragsgr��ten Wert unter allen Samples im Frame und gibt dessen Betragswert zur�ck.
	 * Auf Wunsch wird auch der Kanal und Index der ersten Samples zur�ckgegeben, das diesen
	 * Spitzenwert erreichte (erste Fundstelle).
	 */
	float findPeak( int* piChannel = NULL, int* piPeakIndex = NULL );

	//! Negieren (Multiplikation mit -1 bzw. Phasendrehungum 180�)
	void negate( );

	//! Normalize
	/*
	 * Normalizes all data with regard to the overall maximum peak, i.e. all buffers are
	 * devided by the same value.
	 *
	 * \return Overall peak value that has been used for normalization (can be negative)
	 */
	float Normalize( );
	inline float normalize( ) { return Normalize( ); };

	//! Read/Write Indizierungsoperator f�r Zugriff auf Kanaldaten
	ITASampleBuffer& operator[]( int iChannel );

	//! Read-only Indizierungsoperator
	const ITASampleBuffer& operator[]( int iChannel ) const;

	//! Zuweisungsoperator
	/**
	 * Dieser Operator weist dem Puffer alle Samples eines anderen Quellpuffers zu.
	 * Hierzu wird zun�chst die L�nge des Puffer der des Quellpuffers angepasst.
	 * Anschlie�end werden alle Samples kopiert.
	 */
	ITASampleFrame& operator=( const ITASampleFrame& rhs );

	//! Arithemtische Operatoren (Aliase f�r arithmetische Methoden - siehe oben)
	ITASampleFrame& operator+=( const float rhs );
	ITASampleFrame& operator-=( const float rhs );
	ITASampleFrame& operator*=( const float rhs );
	ITASampleFrame& operator/=( const float rhs );
	ITASampleFrame& operator+=( const ITASampleBuffer& rhs );
	ITASampleFrame& operator-=( const ITASampleBuffer& rhs );
	ITASampleFrame& operator*=( const ITASampleBuffer& rhs );
	ITASampleFrame& operator/=( const ITASampleBuffer& rhs );
	ITASampleFrame& operator+=( const ITASampleFrame& rhs );
	ITASampleFrame& operator-=( const ITASampleFrame& rhs );
	ITASampleFrame& operator*=( const ITASampleFrame& rhs );
	ITASampleFrame& operator/=( const ITASampleFrame& rhs );

	//! Informationen �ber den Puffer als Zeichenkette zur�ckgeben
	std::string toString( ) const;

	//! �berblendfunktionen
	enum
	{
		LINEAR        = 0, //!< Lineare �berblendung aka. Rampe
		COSINE_SQUARE = 1  //!< Cosinus-Quadrat �berblendung (aka Hanning-Fenster)
	};

	//! �berblendrichtungen
	enum
	{
		FADE_IN  = 0, //!< Einblenden
		FADE_OUT = 1  //!< Ausblenden
	};

	enum
	{
		CROSSFADE_TO_SOURCE   = 0, //!< Kreuzblende hin zum Quellsignal
		CROSSFADE_FROM_SOURCE = 1  //!< Kreuzblende weg vom Quellsignal
	};

private:
	int m_iChannels;
	int m_iLength;
	std::vector<ITASampleBuffer> m_vChannels;

	// Alias f�r Iteratoren auf den Kan�len (weniger Schreibarbeit ;-)
	typedef std::vector<ITASampleBuffer>::iterator ch_it;
};

#endif // INCLUDE_WATCHER_ITA_SAMPLE_FRAME
