/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef __ITA_HDFT_SPECTRUM_H__
#define __ITA_HDFT_SPECTRUM_H__

#include <ITABaseDefinitions.h>
#include <ccomplex>
#include <string>
#include <vector>

namespace ITABase
{
	/**
	 * Diese Klasse realisiert half-size DFT-Spektren. Dies sind die DFT-Spektren
	 * rein reellwertiger Signale, wie sie in der Akustik/Audiosignalverarbeitung
	 * �blich sind. Das DFT-Spektrum solcher Signale erf�llt die Hermitische Symetrie.
	 * Daher m�ssen von n komplexwertigen diskreten Fourierkoeffizienten eines
	 * vollst�ndigen DFT-Spektrums nur n/2+1 komplexwertigen Koeffizienten
	 * gespeichert werden.
	 *
	 * Grunds�tzlich werden die komplexwertigen Fourierkoeffizienten im
	 * interleaved Speicherlayout abgelegt, d.h. Re(0), Im(0), Re(1), Im(1), ...
	 *
	 * Die Klasse stellt Methoden f�r die Arbeit mit solchen Spektren bereit.
	 *
	 */

	class ITA_BASE_API CHDFTSpectrum
	{
	public:
		//! Standard-Konstruktor
		CHDFTSpectrum( );

		//! Constructor
		/**
		 * Erzeugt ein Spektrum mit vorgegebener Gr��e und optional zu Null gesetztem Puffer
		 *
		 * \param fSampleRate Sampling rate
		 * \param iDFTSize DFT-Spektrum-Gr��e
		 * \param bZeroInit Setzt den Speicher bei wahr auf Null
		 */
		explicit CHDFTSpectrum( const float fSampleRate, const int iDFTSize, const bool bZeroInit );

		//! Kopierkonstruktor (Zeiger)
		/**
		 * Erzeugt einen unabh�ngiges Spektrum als Kopie des gegebenen Spektrums.
		 * Das neue Spektrum hat die selbe L�nge und enth�lt die gleichen Werte
		 * wie das Quellspektrum.
		 *
		 * \param pSource Zeiger auf das Quellspektrum
		 */
		explicit CHDFTSpectrum( const CHDFTSpectrum* pSource );

		//! Kopierkonstruktor (Referenz)
		/**
		 * Erzeugt einen unabh�ngiges Spektrum als Kopie des gegebenen Spektrums.
		 * Das neue Spektrum hat die selbe L�nge und enth�lt die gleichen Werte
		 * wie das Quellspektrum.
		 *
		 * \param oSource Zeiger auf das Quellspektrum
		 */
		explicit CHDFTSpectrum( const CHDFTSpectrum& oSource );

		//! Destruktor
		virtual ~CHDFTSpectrum( );

		// Initialisieren
		/**
		 * init(0) gibt belegten Speicher frei
		 * vorherige Werte werden grunds�tzlich verworfen!
		 */
		void Init( const int iDFTSize, const bool bZeroinit = true );

		// Gr��e des Spektrums (Anzahl Koeffizienten insgesamt) zur�ckgeben
		// Hinweis: Symmetrische Koeffizienten werden hier nicht mitgez�hlt.
		int GetSize( ) const;

		// L�nge des korrespondieren Zeitsignals zur�ckgeben (Gr��e der DFT)
		int GetDFTSize( ) const;

		// Abtastrate des korrespondieren Zeitsignals zur�ckgeben
		float GetSampleRate( ) const;

		// Abtastrate des korrespondieren Zeitsignals setzen
		void SetSampleRate( const float fSampleRate );

		// Frequenzaufl�sung des Spektrums [Hz] zur�ckgeben
		float GetFrequencyResolution( ) const;

		float GetFrequencyOfBin( const int iBinIndex ) const;

		// Datenzeiger abrufen
		float* GetData( ) const;

		// DFT Koeffizient(en) setzen (Real-/Imagin�rteil bzw. Betrag/Phase)
		void SetCoeffRI( const int iIndex, const float fReal, const float fImag = 0 );
		void SetCoeffsRI( const float fReal, const float fImag = 0 );
		void SetCoeffsRI( const int iOffset, const int iCount, const float fReal, const float fImag = 0 );

		//! Get complex value by index
		std::complex<float> GetCoeff( const int iIndex ) const;

		//! Set coefficient by index
		void SetCoeff( const int iIndex, std::complex<float>& cfCoeff );

		void SetCoeffMP( const int iIndex, const float fMagnitude, const float fPhase );
		void SetCoeffsMP( const float fMagnitude, const float fPhase );
		void SetCoeffsMP( const int iOffset, const int iCount, const float fMagnitude, const float fPhase );

		//! Betragswert setzen, vorhandene Phasen erhalten
		void SetMagnitudePreservePhase( const int iIndex, const float fMagnitude );
		void SetMagnitudesPreservePhases( const float fMagnitude );
		void SetMagnitudesPreservePhases( const int iOffset, const int iCount, const float fMagnitude );

		//! Phase setzen, vorhandene Betr�ge erhalten
		void SetPhasePreserveMagnitude( const int iIndex, const float fPhase );
		void SetPhasesPreserveMagnitudes( const float fPhase );
		void SetPhasesPreserveMagnitudes( const int iOffset, const int iCount, const float fPhase );

		// Konstante addieren
		void Add( const float fReal, const float fImag = 0 );
		void Sub( const float fReal, const float fImag = 0 );

		// Spektrum addieren
		void Add( const CHDFTSpectrum& s );
		void Add( const CHDFTSpectrum* );
		void Sub( const CHDFTSpectrum& s );
		void Sub( const CHDFTSpectrum* );
		void Mul( const CHDFTSpectrum& s );
		void Mul( const float fFactor );
		void Mul( const CHDFTSpectrum* );

		//! Multiplies the conjugate of the given spectrum without data copy
		void MulConj( const CHDFTSpectrum* );

		//! Devide spectrum
		void Div( const CHDFTSpectrum& s );
		void Div( const CHDFTSpectrum* );

		//! Betragsspektrum berechnen und in gegebenes Array speichern
		// Zeiger d�rfen Null sein
		void CalcMagnitudes( float* pfMagnitudes ) const;

		//! Calculates magnitude from real and imaginary part for a given frequency bin
		float CalcMagnitude( const int iIndex ) const;

		//! Phasenspektrum berechnen und in gegebenes Array speichern
		// Zeiger d�rfen Null sein
		void CalcPhases( float* pfPhasess );

		//! Betragsgr��ten Koeffizienten ermitteln
		float FindMax( ) const;
		float FindMax( int& iMaxIndex ) const;

		//! Negieren (Multiplikation mit -1 bzw. Phasendrehungum 180�)
		void Negate( );

		//! Konjugiert das Spectrum
		void Conjugate( );

		//! Set unity DFT coeffs (re == 1, im == 0)
		void SetUnity( );

		//! Set unity DFT coeffs (re == 1, im == 0)
		void SetZero( );

		//! Komplexen bzw. rellen nat�rlichen Logarithmus berechnen
		/**
		 * Reller log entspricht dem Betrag des komplexen log.
		 * Intern wird std::logf verwendet, so dass Betrag 0 den Wert -HUGE_VAL und der relle
		 * Logarithmus f�r negative Werte den Wert NAN zur�ckgibt
		 */
		void Log( const bool bComplex = true );

		//! Komplexe bzw. reelle Exponentialfunktion berechnen
		/**
		 * Intern wird std::exp verwendet, so dass gro�e Betr�ge +-HUGE_VAL zur�ckgeben
		 */
		void Exp( const bool bComplex = true );

		//! Alle Filterkoeffizienten eines anderen Spektrums in dieses kopieren
		void CopyFrom( const CHDFTSpectrum& s );
		void Copy( const CHDFTSpectrum* );

		//! Einen Teil der Filterkoeffizienten eines anderen Spektrums in dieses kopieren
		void CopyFrom( const CHDFTSpectrum& s, const int iOffset, const int iCount );

		//! Zuweisungsoperator
		/**
		 * Dieser Operator weist dem Spektrum eines anderen Spektrums zu.
		 * Hierzu wird zun�chst die Gr��e des Spektrums angepasst.
		 * Anschlie�end werden alle Koeffizienten kopiert.
		 */
		CHDFTSpectrum& operator=( const CHDFTSpectrum& rhs );

		//! Zeichenkette mit den Werten zur�ckgeben
		std::string ToString( );

		//! Calculates the energy of the spectrum
		/**
		 * Returns the energy of a HDFTSpectrum as a float
		 */
		float GetEnergy( ) const;


	private:
		int m_iSize;
		int m_iDFTSize;
		float m_fSampleRate;

		mutable std::vector<float> m_vfData;
	};
} // namespace ITABase

#endif // __ITA_HDFT_SPECTRUM_H__
